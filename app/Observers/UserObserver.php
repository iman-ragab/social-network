<?php
 
namespace App\Observers;
 
use App\User;
 
class UserObserver
{
    /**
     * Handle the User "creating" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function creating(User $user)
    {
        $userAge = \Carbon\Carbon::now()->diffInYears($user->date_of_birth);
        $user->age = $userAge;
    }
}