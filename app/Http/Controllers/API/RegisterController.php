<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\API\BaseController as BaseController;
use App\User;
use App\Helpers\ResponseHelper;

class RegisterController extends BaseController  
{
    public function register(Request $request)
    {   
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
            'date_of_birth' => 'required|date|date_format:Y-m-d',
            'image' => 'required|image',
        ]);

        if ($validator->fails()) {
            return $this->sendError($validator->errors(), ResponseHelper::HTTP_UNPROCESSABLE_ENTITY);
        }

        $user = User::create($data);    

        return $this->sendSuccess(compact('user'));
    }    
}
