<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseHelper;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Controllers\API\BaseController as BaseController;
use Illuminate\Support\Facades\Validator;

class AuthController extends BaseController
{
    use AuthenticatesUsers;

    protected $maxLoginAttempts = 5;
    protected $decayMinutes = 30;



    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $validator = Validator::make(request()->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError($validator->errors(), ResponseHelper::HTTP_UNPROCESSABLE_ENTITY);
        }

        $credentials = request()->only('email', 'password');

        if ($this->hasTooManyLoginAttempts(request())) {
            $this->fireLockoutEvent(request());
            return $this->sendError(['Too many logins'], ResponseHelper::HTTP_BAD_REQUEST);
        }

        try {
            if (!$token = auth()->attempt($credentials)) {
                $this->incrementLoginAttempts(request());
                return $this->sendError(['Unauthorized'], ResponseHelper::HTTP_UNAUTHORIZED);
            }
        } catch (JWTException $e) {
            // something went wrong
            $this->incrementLoginAttempts(request());
            return $this->sendError(['Could Not Create Token'], ResponseHelper::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->respondWithToken($token);
    }


    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return $this->sendSuccess();
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return $this->sendSuccess([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ]);
    }
}
