<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\User;
use App\Helpers\ResponseHelper;
use Illuminate\Support\Facades\Auth;
use App\Follow;
use App\Repository\UserRepositoryInterface;
use App\Repository\TweetRepositoryInterface;

class UserController extends BaseController  
{
    private $userRepository;
    private $tweetRepository;

    public function __construct(UserRepositoryInterface $userRepository, TweetRepositoryInterface $tweetRepository)
    {
        $this->userRepository = $userRepository;
        $this->tweetRepository = $tweetRepository;
    }

    /**
     * User follow another user.
     *
     * @param  Request  $request
     * @param  integer  $id
     * @return Response
     */
    public function follow(Request $request, $id)
    {   
        $user = $this->userRepository->find($id);
        $isAlreadyFollowing = $this->userRepository->isUserFollowing(Auth::id(), $id);

        if (!$user) {
            return $this->sendError(['User not found'], ResponseHelper::HTTP_NOT_FOUND);
        } else if ($id == Auth::id()) {
            return $this->sendError(['You can not follow yourself'], ResponseHelper::HTTP_UNAUTHORIZED);
        } else if ($isAlreadyFollowing) {
            return $this->sendError(['You are already following this user'], ResponseHelper::HTTP_UNAUTHORIZED);
        }

        $data['user_id'] = $id;
        $data['follower_id'] = Auth::id();

        Follow::create($data);    

        return $this->sendSuccess();
    }  

    /**
     * Get user timeline
     *
     * @return Response
     */
    public function timeline()
    {   
        $timeline = $this->tweetRepository->getUserTimeLine(Auth::id());
        return $this->sendSuccess($timeline);
    }  
}
