<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => ['api'],
    'prefix' => '{locale}/auth',
    'where' => ['locale' => 'en|ar']
], function ($router) {
    Route::post('register', 'API\RegisterController@register');
    Route::post('login', 'API\AuthController@login',  ['name' => 'login']);
    Route::middleware(['jwt.verify'])->post('logout', 'API\AuthController@logout');
    Route::middleware(['jwt.verify'])->post('refresh', 'API\AuthController@refresh');
});

Route::group([
    'middleware' => ['api', 'jwt.verify'],
    'prefix' => '{locale}/me',
    'where' => ['locale' => 'en|ar']
], function ($router) {
    Route::post('tweets', 'API\TweetController@store');
    Route::delete('tweets/{tweet}', 'API\TweetController@destroy');
    Route::get('timeline', 'API\UserController@timeline');
});

Route::group([
    'middleware' => ['api', 'jwt.verify'],
    'prefix' => '{locale}/users',
    'where' => ['locale' => 'en|ar']
], function ($router) {
    Route::post('{user}/follow', 'API\UserController@follow');
});
